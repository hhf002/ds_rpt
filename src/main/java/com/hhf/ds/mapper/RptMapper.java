package com.hhf.ds.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhf.ds.entity.Rpt;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RptMapper extends BaseMapper<Rpt> {
}
