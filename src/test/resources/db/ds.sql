-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        10.3.13-MariaDB - mariadb.org binary distribution
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 导出 ds 的数据库结构
CREATE DATABASE IF NOT EXISTS `ds` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;
USE `ds`;

-- 导出  表 ds.bi_ds 结构
CREATE TABLE IF NOT EXISTS `bi_ds` (
  `ds_Id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `ds_code` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据源编码',
  `ds_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据源名称',
  `ds_desc` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
  `ds_type` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据库类型',
  `jdbc_url` varchar(300) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'jdbc驱动Url',
  `username` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据库账号',
  `password` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '数据库密码',
  `driver_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'jdbc驱动类路径',
  `ds_params` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'jdbcUrl中存在的动态参数',
  `state` tinyint(4) DEFAULT NULL COMMENT '1有效，0无效',
  `created_by` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`ds_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='数据源信息';

-- 数据导出被取消选择。

-- 导出  表 ds.bi_rpt 结构
CREATE TABLE IF NOT EXISTS `bi_rpt` (
  `rpt_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `rpt_code` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '报表编码',
  `rpt_name` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '报表名称',
  `ds_id` int(11) DEFAULT NULL COMMENT '数据源id',
  `rpt_expr` text COLLATE utf8mb4_bin DEFAULT NULL COMMENT '报表xml配置数据',
  `state` tinyint(4) DEFAULT NULL COMMENT '1有效，0无效',
  `created_by` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`rpt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 数据导出被取消选择。

CREATE TABLE `t_i18n` (
      `id` VARCHAR(36) NOT NULL COLLATE 'utf8_general_ci',
      `bundle` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
      `locale` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
      `code` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
      `msg` VARCHAR(300) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
      `sort` INT(11) NULL DEFAULT NULL,
      `state` TINYINT(4) NULL DEFAULT NULL,
      PRIMARY KEY (`id`) USING BTREE
)
    COLLATE='utf8_general_ci'
    ENGINE=InnoDB
;

INSERT INTO `t_i18n` (`id`, `bundle`, `locale`, `code`, `msg`, `sort`, `state`) VALUES ('1', 'base', 'zh_CN', '10010', '已发布过的问卷类型，问卷名“{0}”，请取消后再发布更新', 1, 1);
INSERT INTO `t_i18n` (`id`, `bundle`, `locale`, `code`, `msg`, `sort`, `state`) VALUES ('2', 'base', 'zh_CN', '10011', '已发布过的问卷类型，问卷名“{0}”，请取消后再发布', 1, 1);


/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
