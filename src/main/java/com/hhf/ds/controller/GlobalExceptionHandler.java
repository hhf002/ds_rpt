package com.hhf.ds.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	/**
	 * 处理自定义的业务异常
	 * @param req
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value = BizException.class)
	@ResponseBody
	public  ResponseResult bizExceptionHandler(HttpServletRequest req, BizException e){
		log.error("发生业务异常！原因是：{}",e.getErrorMsg());
		String errorCode = e.getErrorCode();
		String errorMsg = e.getErrorMsg();
		return ResponseResult.failure(errorCode, errorMsg);
	}

	/**
	 * 处理自定义的业务异常
	 * @param req
	 * @param e
	 * @return
	 */
    @ExceptionHandler(value = RuntimeException.class)
    @ResponseBody
	public  ResponseResult runtimeExceptionHandler(HttpServletRequest req, RuntimeException e){
    	log.error("发生业务异常！", e);
    	return ResponseResult.failure(ResponseStatus.INTERNAL_SERVER_ERROR.getCode(),e.getMessage());
    }

	/**
	 * 处理空指针的异常
	 * @param req
	 * @param e
	 * @return
	 */
	@ExceptionHandler(value =NullPointerException.class)
	@ResponseBody
	public ResponseResult exceptionHandler(HttpServletRequest req, NullPointerException e){
		log.error("发生空指针异常！原因是:",e);
		return ResponseResult.failure(ResponseStatus.BAD_REQUEST.getCode(), e.getMessage());
	}


    /**
     * 处理其他异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =Exception.class)
	@ResponseBody
	public ResponseResult exceptionHandler(HttpServletRequest req, Exception e){
    	log.error("未知异常！原因是:",e);
       	return ResponseResult.failure(ResponseStatus.INTERNAL_SERVER_ERROR.getCode(), e.getMessage());
    }
}
