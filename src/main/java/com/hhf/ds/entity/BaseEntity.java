package com.hhf.ds.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BaseEntity {
    private String createdBy;
    private LocalDateTime created;
    private String updatedBy;
    private LocalDateTime updated;
}
