package com.hhf.ds.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hhf.ds.entity.Ds;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DsMapper extends BaseMapper<Ds> {
}
