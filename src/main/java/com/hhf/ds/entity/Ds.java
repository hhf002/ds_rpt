package com.hhf.ds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("BI_DS")
@Data
public class Ds extends BaseEntity{
    @TableId(type = IdType.AUTO)
    private Integer dsId;
    /** 数据源编码 */
    private String dsCode;

    /** 数据源名称 */
    private String dsName;

    /** 数据源描述 */
    private String dsDesc;

    /** 数据源类型 mysql，orace，sqlserver */
    private String dsType;

    private String jdbcUrl;

    /** 关系型数据库用户名 */
    private String username;

    /** 关系型数据库密码 */
    private String password;

    /** 关系型数据库驱动类 */
    private String driverName;

    /** 传入的自定义参数，解决url中存在的动态参数*/
    private String dsParams;

    /** 0--禁用 1--启用 */
    private Integer state;


}
