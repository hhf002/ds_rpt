package com.hhf.ds.controller;

import com.hhf.ds.entity.Ds;
import com.hhf.ds.entity.Rpt;
import com.hhf.ds.service.DsService;
import com.hhf.ds.service.RptService;
import com.hhf.ds.util.CoreAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private RptService rptService;

    @Autowired
    private DsService dsService;

    @GetMapping
    public String getDs(HttpServletRequest request, Ds ds) {
        List<Ds> dsList = dsService.getDs(ds);
        request.setAttribute("dsList", dsList);
        return "/dsList";
    }

    @GetMapping("/dsEdit")
    public String editDs(HttpServletRequest request, Integer dsId) {
        Ds ds = dsService.getDsById(dsId);
        request.setAttribute("ds",ds);
        return "/dsEdit";
    }
    @GetMapping("/dsAdd")
    public String addDs(HttpServletRequest request) {
        request.setAttribute("ds",new Ds());
        return "/dsEdit";
    }
    @PostMapping("/dsSave")
    public String saveDs(Ds ds) {
        if(ds.getDsId() == null) {
            ds.setCreated(LocalDateTime.now());
            ds.setCreatedBy("1");
            ds.setState(1);
        }
        ds.setUpdated(LocalDateTime.now());
        ds.setUpdatedBy("1");
        dsService.saveOrUpdate(ds);
        dsService.refreshDsCache(ds.getDsId());
        return "redirect:/admin/";
    }
    @PostMapping("/dsDel")
    @ResponseBody
    public ResponseResult delDs(HttpServletRequest request, Integer dsId) {
        dsService.removeById(dsId);
        dsService.removeDsCache(dsId);
        List<Ds> dsList = dsService.getDs(new Ds());
        request.setAttribute("dsList", dsList);
        return ResponseResult.success();
    }

    /**************************/
    @GetMapping("/rptList")
    public String getRpt(HttpServletRequest request, Rpt rpt) {
        List<Rpt> rptList = rptService.getRpt(rpt);
        request.setAttribute("rptList", rptList);
        return "/rptList";
    }

    @GetMapping("/rptEdit")
    public String editRpt(HttpServletRequest request, String rptCode) {
        Rpt rpt = rptService.getRptByRptCode(rptCode);
        request.setAttribute("rpt",rpt);
        return "/rptEdit";
    }
    @GetMapping("/rptAdd")
    public String addRpt(HttpServletRequest request) {
        request.setAttribute("rpt",new Rpt());
        return "/rptEdit";
    }
    @PostMapping("/rptSave")
    public String saveRpt(Rpt rpt) {
        String rptCode = rpt.getRptCode();
        CoreAssert.notBlank(rptCode, "rptCode为空");
        if(rpt.getRptId() == null) {
            rpt.setCreated(LocalDateTime.now());
            rpt.setCreatedBy("1");
            rpt.setState(1);
        }
        rpt.setUpdated(LocalDateTime.now());
        rpt.setUpdatedBy("1");
        rptService.saveOrUpdate(rpt);
        rptService.refreshRptCache(rptCode);
        return "redirect:/admin/rptList";
    }
    @PostMapping("/rptDel")
    @ResponseBody
    public ResponseResult delRpt(HttpServletRequest request, Integer rptId, String rptCode) {
        rptService.removeById(rptId);
        rptService.removeRptCache(rptCode);
        List<Rpt> rptList = rptService.getRpt(new Rpt());
        request.setAttribute("rptList", rptList);
        return ResponseResult.success();
    }
}
