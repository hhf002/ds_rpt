# ds_rpt

#### 介绍
多数据源报表接口管理器，是https://gitee.com/hhf002/rpt.git 的升级版本。升级后报表支持多数据源，可在线即时配置，免去到服务器上修改配置文件的麻烦

#### 软件架构
+ JDK8+
+ springboot2.6
+ MySQL/MariaDB

#### 安装说明
* 1. 数据库环境准备(目前只支持MySQL/mariaDB), 执行数据库脚本ds/src/test/resources/db/ds.sql，在application.yml中配置spring.datasource.url、spring.datasource.username、spring.datasource.password等元数据数据库信息
* 2、idea中执行com.hhf.ds.Application
* 3、浏览器打开后台配置页面，url: http://localhost:8888/admin/
* 4、新增MySQL/mariaDB数据源，如图
![数据源图](./ds.png)
* 5、配置报表接口，url: http://localhost:8888/admin/rptList 
报表的配置内容和https://gitee.com/hhf002/rpt.git的xml类似，配置效果如图
![报表配置图](./rpt.png)

#### 使用说明
使用方式也和https://gitee.com/hhf002/rpt.git一致
1.  通用报表接口传参参考
```
POST http://localhost:8888/rpt/601/list
Content-Type: application/json

{
  "rptCode":"RPT_TEST",
  "pageSize":"",
  "pageNo":"",
  "msg":"异常"
}
###
```
2.  访问通用Excel导出接口
```
POST http://localhost:8888/rpt/602/exp
Content-Type: application/json

{
  "rptCode":"RPT_TEST",
  "msg":"异常"
}
###
```
3.  参数简介
+ rptCode：报表编码，一张报表有唯一的一个编码。
+ pageSize：分页参数，每页展示条数，不传不分页
+ pageNo：分页参数，展示第几页，不传不分页
+ msg：自定义的业务SQL查询字段



