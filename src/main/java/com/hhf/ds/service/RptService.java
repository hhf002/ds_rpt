package com.hhf.ds.service;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hhf.ds.entity.Ds;
import com.hhf.ds.entity.Rpt;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface RptService extends IService<Rpt> {
    DruidDataSource getDataSourceByRptCode(String rptCode);
    Connection getConnByRptCode(String rptCode) throws SQLException;
    Rpt getRptByRptCode(String rptCode);
    ByteArrayInputStream getRptExprStream(String rptCode);
    void refreshRptCache(String rptCode);
    void removeRptCache(String rptCode);

    List<Rpt> getRpt(Rpt rpt);
}
