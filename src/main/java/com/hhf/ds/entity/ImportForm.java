package com.hhf.ds.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ImportForm {
    @NotNull
    private String rptCode;
    @NotNull
    private String filePath;
}
