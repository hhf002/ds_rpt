package com.hhf.ds.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhf.ds.entity.Ds;
import com.hhf.ds.mapper.DsMapper;
import com.hhf.ds.service.DsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class DsServiceImpl extends ServiceImpl<DsMapper, Ds> implements DsService {
    Map<Integer,Ds> dsCache = new ConcurrentHashMap();

    @Override
    public Ds getDsById(Integer dsId) {
        if(dsCache.containsKey(dsId)) {
            return dsCache.get(dsId);
        }
        LambdaQueryWrapper<Ds> qw = new LambdaQueryWrapper();
        qw.eq(Ds::getDsId,dsId);
        qw.eq(Ds::getState,1);
        qw.last("limit 1");
        Ds ds = getOne(qw);
        dsCache.put(dsId, ds);
        return ds;
    }

    @Override
    public void refreshDsCache(Integer dsId) {
        LambdaQueryWrapper<Ds> qw = new LambdaQueryWrapper();
        qw.eq(dsId != null, Ds::getDsId, dsId);
        qw.eq(Ds::getState,1);
        List<Ds> dsList = baseMapper.selectList(qw);
        for(Ds ds : dsList) {
            dsCache.put(dsId, ds);
        }
    }

    @Override
    public void removeDsCache(Integer dsId) {
        dsCache.remove(dsId);
    }

    @Override
    public List<Ds> getDs(Ds ds) {
        QueryWrapper<Ds> qw = new QueryWrapper();
        Integer dsId = ds.getDsId();
        String dsCode = ds.getDsCode();
        String dsName = ds.getDsName();
        Integer state = ds.getState();
        qw.eq(dsId != null,"DS_ID", dsId);
        qw.eq(StringUtils.isNotBlank(dsCode),"DS_CODE", dsCode);
        qw.eq(StringUtils.isNotBlank(dsName),"DS_NAME", dsName);
        qw.eq(state != null,"STATE", state);
        return list(qw);
    }

}
