package com.hhf.ds.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.hhf.ds.**.mapper"})
public class MybatisPlusConfig {
}
