package com.hhf.ds.service.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hhf.ds.entity.Ds;
import com.hhf.ds.entity.Rpt;
import com.hhf.ds.mapper.RptMapper;
import com.hhf.ds.service.DsService;
import com.hhf.ds.service.PoolService;
import com.hhf.ds.service.RptService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class RptServiceImpl extends ServiceImpl<RptMapper, Rpt> implements RptService {
    @Autowired
    private PoolService poolService;
    @Autowired
    private DsService dsService;

    Map<String,Rpt> rptCache = new ConcurrentHashMap();

    @Override
    public Rpt getRptByRptCode(String rptCode) {
        if(rptCache.containsKey(rptCode)) {
            return rptCache.get(rptCode);
        }
        LambdaQueryWrapper<Rpt> qw = new LambdaQueryWrapper();
        qw.eq(Rpt::getRptCode,rptCode);
        qw.eq(Rpt::getState,1);
        Rpt rpt = baseMapper.selectOne(qw);
        rptCache.put(rptCode,rpt);
        return rpt;
    }

    @Override
    public void refreshRptCache(String rptCode) {
        LambdaQueryWrapper<Rpt> qw = new LambdaQueryWrapper();
        qw.eq(StringUtils.isNotBlank(rptCode), Rpt::getRptCode,rptCode);
        qw.eq(Rpt::getState,1);
        List<Rpt> rptList = baseMapper.selectList(qw);
        for(Rpt rpt : rptList) {
            rptCache.put(rptCode, rpt);
        }
    }

    @Override
    public void removeRptCache(String rptCode) {
        rptCache.remove(rptCode);
    }

    @Override
    public ByteArrayInputStream getRptExprStream(String rptCode) {
        Rpt rpt = getRptByRptCode(rptCode);
        Assert.notNull(rpt,"报表"+rptCode+"不存在");
        String expr = rpt.getRptExpr();
        ByteArrayInputStream in= new ByteArrayInputStream(expr.getBytes());
        return in;
    }

    @Override
    public Connection getConnByRptCode(String rptCode) throws SQLException {
        Rpt rpt = getRptByRptCode(rptCode);
        Assert.notNull(rpt,"报表"+rptCode+"不存在");
        Ds ds = dsService.getDsById(rpt.getDsId());
        return poolService.getPooledConnection(ds);
    }

    @Override
    public DruidDataSource getDataSourceByRptCode(String rptCode) {
        Rpt rpt = getRptByRptCode(rptCode);
        Assert.notNull(rpt,"报表"+rptCode+"不存在");
        Ds ds = dsService.getDsById(rpt.getDsId());
        return poolService.getJdbcConnectionPool(ds);
    }


    @Override
    public List<Rpt> getRpt(Rpt rpt) {
        QueryWrapper<Rpt> qw = new QueryWrapper();
        Integer dsId = rpt.getDsId();
        Integer rptId = rpt.getRptId();
        String rptCode = rpt.getRptCode();
        String rptName = rpt.getRptName();
        Integer state = rpt.getState();
        qw.eq(dsId != null,"DS_ID", dsId);
        qw.eq(rptId != null,"RPT_ID", rptId);
        qw.eq(StringUtils.isNotBlank(rptCode),"RPT_CODE", rptCode);
        qw.eq(StringUtils.isNotBlank(rptName),"RPT_NAME", rptName);
        qw.eq(state != null,"STATE", state);
        return list(qw);
    }

}
