package com.hhf.ds.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hhf.ds.entity.Ds;

import java.util.List;

public interface DsService extends IService<Ds> {
    Ds getDsById(Integer dsId);
    void refreshDsCache(Integer dsId);
    void removeDsCache(Integer dsId);
    List<Ds> getDs(Ds ds);
}
