package com.hhf.ds.controller;

import com.hhf.ds.entity.ImportForm;
import com.hhf.ds.entity.PageBean;
import com.hhf.ds.service.DsService;
import com.hhf.ds.service.ImportService;
import com.hhf.ds.service.ListExpSupport;
import com.hhf.ds.service.RptService;
import net.sf.jsqlparser.JSQLParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

/**
 * @author haohaifeng
 * @date 2021/1/22 15:46
 */
@RestController
@RequestMapping(value = "/rpt")
public class RptController {

    @Autowired
    private DsService dsService;
    @Autowired
    private RptService rptService;
    @Autowired
    private ImportService importService;

    @Autowired
    private ListExpSupport listExpSupport;

    @RequestMapping(value = "/601/list", method = RequestMethod.POST)
    public ResponseResult<PageBean> list(@RequestBody Map<String, Object> queryArgs) {
        PageBean data = listExpSupport.pageList(queryArgs);
        return ResponseResult.success(data);
    }
    @RequestMapping(value = "/602/exp", method = RequestMethod.POST)
    public void exp(HttpServletResponse response, @RequestBody Map<String, Object> queryArgs) throws IOException, SQLException {
        listExpSupport.exp(response, queryArgs);
    }

    @GetMapping("/600/rptSql/{rptCode}")
    public String getRptSql(@PathVariable String rptCode) {
        return listExpSupport.getExecSql(rptCode);
    }

    @GetMapping(value = "/600/list")
    public ResponseResult<PageBean> getList(@RequestParam Map<String, Object> queryArgs) {
        PageBean data = listExpSupport.pageList(queryArgs);
        return ResponseResult.success(data);
    }
    @GetMapping(value = "/600/exp")
    public void doExp(HttpServletResponse response, @RequestParam Map<String, Object> queryArgs) throws IOException, SQLException {
        listExpSupport.exp(response, queryArgs);
    }

    @GetMapping(value = "/600/refreshAllCache")
    public ResponseResult refreshCache() {
        dsService.refreshDsCache(null);
        rptService.refreshRptCache(null);
        return ResponseResult.success();
    }

    @PostMapping(value = "/600/imp")
    public ResponseResult imp(@RequestBody ImportForm importForm) throws JSQLParserException, IOException {
        importService.execImp(importForm.getRptCode(), importForm.getFilePath());
        return ResponseResult.success();
    }

}
