package com.hhf.ds.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

public class CoreAssert extends Assert {

	public static void notBlank(String arg, String message) {
		if (StringUtils.isBlank(arg)) {
			throw new IllegalArgumentException(message);
		}
	}
}
