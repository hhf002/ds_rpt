package com.hhf.ds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 查询主体
 */
@TableName("BI_RPT")
@Data
public class Rpt extends BaseEntity {
    @TableId(type = IdType.AUTO)
    private Integer rptId;
    private String rptCode;
    private String rptName;
    private Integer dsId;
    private String rptExpr;
    private Integer state;


}
